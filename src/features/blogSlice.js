import { createSlice } from "@reduxjs/toolkit";

const initialStateValue = {
  title: "",
  content: "",
  tag: "",
  featured: false,
  blogImage: "",
};

export const blogSlice = createSlice({
  name: "blogSlice",
  initialState: initialStateValue,
  reducers: {
    changeTitle: (state, action) => {
      state.title = action.payload;
    },
    changeContent: (state, action) => {
      state.content = action.payload;
    },
  },
});
export const { changeTitle, changeContent } = blogSlice.actions;
export default blogSlice.reducer;
